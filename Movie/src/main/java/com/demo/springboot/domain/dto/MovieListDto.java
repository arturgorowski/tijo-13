package com.demo.springboot.domain.dto;

import java.io.Serializable;
import java.util.List;

public class MovieListDto implements Serializable {

    private List<MovieDto> movies;

    public MovieListDto(List<MovieDto> movies) {
        this.movies = movies;
    }
    public List<MovieDto> getMovies() {
        return movies;
    }
}
