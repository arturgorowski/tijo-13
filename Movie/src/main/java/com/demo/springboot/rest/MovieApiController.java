package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.demo.springboot.service.MovieService;

@Controller
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    MovieService service;

    @GetMapping(value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies(@RequestParam(defaultValue = "") String search) {

        LOGGER.info("--- get movies");

        String csvFile = "src/main/resources/movies.csv";

        return service.getMovies(csvFile, search);
    }

    @RequestMapping(value = "/movies/{id}", method=RequestMethod.PUT)
    @ResponseBody
    @CrossOrigin
    public ResponseEntity modifyMovie(@PathVariable(value="id") final int movieId, @RequestParam("title") String title, @RequestParam("year") int year, @RequestParam("image") String image){
        MovieDto movie = new MovieDto(movieId,title,year,image);
        LOGGER.info("--- put movie");
        String csvFile = "src/main/resources/movies.csv";

        boolean modifyMovie = service.modifyMovie(movie, csvFile);

        if (modifyMovie){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
